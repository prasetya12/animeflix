import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  FlatList,
  StyleSheet,
  Alert,
  ScrollView,
  ActivityIndicator,
  WebView
} from "react-native";
import {
  Container,
  ListItem,
  List,
  Left,
  Right,
  Body,
  Button
} from "native-base";
import VideoPlayer from "react-native-video-controls";
import IconM from "react-native-vector-icons/MaterialCommunityIcons";
import CardFavorite from "../components/CardFavorite";
import { EPISODE, DETAIL_VIDEO, CATEGORY } from "../actions/video";
import { connect } from "react-redux";
import ReadMore from "react-native-read-more-text";
import { _ } from "lodash";
import Shimmer from "./Shimmer";
import ScrollableTabView from "react-native-scrollable-tab-view";
import axios from "axios";
import cheerio from "cheerio-without-node-native";
import Orientation from "react-native-orientation";

class VideoScreen extends React.Component {
  async componentDidMount() {
    Orientation.lockToPortrait();
    series = this.props.navigation.getParam("series", false);

    category =
      this.props.detail.data.category.split(",") &&
      this.props.detail.data.category.split(",");

    await this.props.dispatch(EPISODE(series));
    this.props.dispatch(CATEGORY(category[0], 7));

    if (!this.state.slug) {
      this.setState({
        slug: series.replace(/\s+/g, "-").toLowerCase() + "-episode-1"
      });
    }

    this.props.dispatch(DETAIL_VIDEO(this.state.slug));
  }

  constructor(props) {
    super(props);
    this.state = {
      favorite: true,
      slug: props.navigation.getParam("slug", false),
      video: props.navigation.getParam("video", false),
      fullscreen: false
    };
  }

  button() {
    Alert.alert(
      "Removing Boruto?",
      "You'll not longer get notification from this anime ",
      [
        {
          text: "NO",
          onPress: () => this.setState({ favorite: false }),
          style: "cancel"
        },
        { text: "YES", onPress: () => this.setState({ favorite: true }) }
      ]
    );
  }
  ShowHideTextComponentView = () => {
    if (this.state.favorite == true) {
      this.setState({ favorite: false });
    } else {
      this.button();
    }
  };

  listEmpty = () =>
    [1, 2, 3, 4].map((data, item) => (
      <ListItem
        key={item}
        style={{
          borderBottomWidth: 0,
          marginLeft: 0,
          marginHorizontal: 10
        }}
      >
        <Left>
          <Shimmer
            autoRun={true}
            style={{
              flex: 1,
              width: 20,
              height: 80,
              resizeMode: "cover",
              borderRadius: 10,
              marginLeft: 20,
              marginRight: 20
            }}
          />
        </Left>
        <Body>
          <Shimmer autoRun={true} style={{ paddingRight: 20, width: 80 }} />
        </Body>
      </ListItem>
    ));

  episode = async (slug, series) => {

    await this.setState({ slug });
    await this.props.dispatch(DETAIL_VIDEO(slug));
    await this.props.dispatch(EPISODE(series));

    axios.get(this.props.detail.data.video_url).then(response => {
      const $ = cheerio.load(response.data);
      const video = $("source").attr("src");
      this.setState({ video });
    });
  };

  render() {
    return (
      <Container style={{ flex: 1, backgroundColor: "#232426" }}>
        <View style={{ flex: 1, marginTop: 20 }}>
          {/* <WebView
            source={{ uri: this.props.detail.data.video_url }}
            style={{ flex: 1 }}
            title={this.props.detail.data.title}
            onBack={() => null}
            originWhitelist={["http://153.92.5.103/"]}
          /> */}
          <VideoPlayer
            fullscreen={this.state.fullscreen}
            onEnterFullscreen={() => {
              Orientation.lockToLandscape();
              this.setState({ fullscreen: true });
            }}
            onExitFullscreen={() => {
              Orientation.lockToPortrait();
              this.setState({ fullscreen: false });
            }}
            paused={true}
            onBack={() => this.props.navigation.push("Parallax")}
            ref={ref => {
              this.player = ref;
            }}
            source={{ uri: this.state.video }}
            style={{ width: null, height: 300 }}
          />
        </View>

        {this.state.fullscreen || (
          <View style={{ flex: 2 }}>
            {/* <ScrollableTabView
              tabBarBackgroundColor="#232426"
              tabBarActiveTextColor="#D8368C"
              tabBarInactiveTextColor="grey"
              tabBarTextStyle={{ fontFamily: "Roboto-Medium", fontSize: 14 }}
              tabBarUnderlineStyle={{
                backgroundColor: "#D8368C",
                borderBottomColor: "red"
              }}
            >
              <ScrollView
                tabLabel="Episodes"
                style={{ backgroundColor: "#232426" }}
              > */}

            <View style={{ borderBottomColor: "#D8368C" }}>
              <ScrollView showsVerticalScrollIndicator={false}>
                <Text style={styles.title}>Description</Text>
                <ReadMore numberOfLines={4}>
                  <Text style={styles.text}>
                    {this.props.detail.data.description}
                  </Text>
                </ReadMore>

                  <Text style={styles.title}>Episode</Text>
                  <FlatList
                    data={this.props.episode.results}
                    ListEmptyComponent={this.listEmpty}
                    renderItem={({ item }) => (
                      <ListItem
                        style={{
                          borderBottomWidth: 0,
                          marginLeft: 0,
                          backgroundColor:
                            this.state.slug == item.slug ? "#515154" : "#232426"
                        }}
                        onPress={() => this.episode(item.slug, item.series)}
                      >
                        <Left>
                          <Image
                            source={{ uri: item.image_url }}
                            style={{
                              flex: 1,
                              width: 80,
                              height: 80,
                              resizeMode: "cover",
                              borderRadius: 10,
                              marginLeft: 20
                            }}
                          />
                        </Left>
                        <Body style={{ marginLeft: 10 }}>
                          <Text
                            style={{
                              color: "#dedee6",
                              fontWeight: "bold"
                            }}
                          >
                            Episode {item.episode}
                          </Text>
                          <Text style={{ color: "#dedee6" }}>24 minute</Text>
                        </Body>
                        <Right>
                          <Button transparent>
                            <IconM
                              name="eye"
                              style={{ color: "#dedee6" }}
                              size={20}
                            />
                          </Button>
                        </Right>
                      </ListItem>
                    )}
                    keyExtractor={item => `index${item.id}`}
                    showsHorizontalScrollIndicator={false}
                  />

              </ScrollView>
            </View>
            {/* </ScrollView>
              <ScrollView
                tabLabel="Description"
                style={{ backgroundColor: "#232426" }}
              > */}
            <View>
              <View
                style={{
                  borderTopWidth: 3,
                  borderTopColor: "#D8368C",
                  marginTop: 20
                }}
              >
                <View
                  style={{
                    height: 30,
                    width: 110,
                    backgroundColor: "#232426",
                    marginTop: -15
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Roboto-Medium",
                      fontSize: 18,
                      color: "#D8368C",
                      marginLeft: 10
                    }}
                  >
                    Related
                  </Text>
                </View>
                <View>
                  <ScrollView
                    horizontal
                    style={{ marginLeft: 20, marginTop: 10 }}
                    showsHorizontalScrollIndicator={false}
                  >
                    <FlatList
                      horizontal={true}
                      showsHorizontalScrollIndicator={false}
                      data={this.props.category.results}
                      renderItem={({ item }) => (
                        <CardFavorite item={item} {...this.props} />
                      )}
                      keyExtractor={item => `index${item.id}`}
                    />
                  </ScrollView>
                </View>
              </View>
            </View>
            {/* </ScrollView>
            </ScrollableTabView> */}
            <Button
              disabled={!this.props.navigation.getParam("isLogin", false)}
              onPress={this.ShowHideTextComponentView}
              iconRight
              full
              style={{
                backgroundColor: !this.props.navigation.getParam(
                  "isLogin",
                  false
                )
                  ? "grey"
                  : this.state.favorite
                  ? "#D8368C"
                  : "white"
              }}
            >
              {this.state.favorite ? (
                <Text
                  style={{
                    color: "#fff",
                    fontFamily: "Robotic",
                    fontWeight: "bold"
                  }}
                >
                  Add to Favorite
                </Text>
              ) : (
                <Text style={{ fontFamily: "Robotic", fontWeight: "bold" }}>
                  Already in Favorite{" "}
                  <Image
                    source={require("../assets/icon/done.png")}
                    style={{ height: 12, width: 12, marginLeft: 5 }}
                  />
                </Text>
              )}
            </Button>
          </View>
        )}
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  video: {
    width: 360,
    height: 200,
    alignSelf: "center"
  },
  texttab: {
    color: "#B8B4B4",
    fontWeight: "bold",
    fontFamily: "Roboto"
  },
  icon: {
    width: 300,
    height: 300,
    alignSelf: "center"
  },
  title: {
    marginTop: 10,
    paddingVertical: 5,
    color: "#E0E0E0",
    fontFamily: "Roboto-Bold",
    fontSize: 16,
    fontWeight: "500"
  },
  text: {
    color: "#7D7D7D",
    fontSize: 12,
    textAlign: "justify"
  }
});

const mapStateToProps = state => ({
  episode: state.episodeReducers,
  detail: state.videoReducers,
  category: state.categoryReducers
});

export default connect(mapStateToProps)(VideoScreen);
