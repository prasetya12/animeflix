const HEADER_MAX_HEIGHT = 300;

import { StyleSheet } from "react-native";

const textStyle = StyleSheet.create({
  fill: {
    flex: 1,
    backgroundColor: "#101010"
  },
  content: {
    flex: 1
  },
  header: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: "#101010",
    overflow: "hidden",
    height: HEADER_MAX_HEIGHT
  },
  backgroundImage: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: 250,
    resizeMode: "contain"
  },
  linearGradient: {
    height: 180,
    width: null,
    paddingLeft: 15,
    paddingRight: 15,
    position: "relative"
  },
  bar: {
    backgroundColor: "transparent",
    marginTop: 38,
    height: 32,
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    top: 0,
    left: 0,
    right: 0
  },
  title: {
    marginTop: 10,
    paddingHorizontal: 5,
    color: "#E0E0E0",
    fontFamily: "Roboto-Bold",
    fontSize: 16,
    fontWeight: "500"
  },
  scrollViewContent: {
    paddingTop: HEADER_MAX_HEIGHT
  },
  row: {
    height: 40,
    margin: 16,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center"
  },
  backgroundVideo: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    height: null,
    width: null,
    marginRight: 5
  },
  talkBubbleTriangle: {
    position: "absolute",
    bottom: -26,
    left: 45,
    top: 150,
    width: 0,
    height: 0,
    borderTopColor: "#fff",
    borderTopWidth: 8,
    borderRightWidth: 12,
    borderLeftWidth: 12,
    borderLeftColor: "transparent",
    borderRightColor: "transparent",
    borderBottomWidth: 26,
    borderBottomColor: "transparent"
  }
});

export default textStyle;
